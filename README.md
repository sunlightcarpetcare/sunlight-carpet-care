Sunlight Carpet Care provides quality floor cleaning services for both residential and commercial. We are a local IICRC certified family owned and operated business with over 20+ years experience serving the Central Coast.

Address: 1448 Sonya Lane, Santa Maria, CA 93458, USA

Phone: 805-720-8240

Website: https://sunlightcarpetcare.co
